# used to have one source for directory structure,
# array elements can be renamed,
# changing position in array breaks relations,
# for reverse searches dirlevels are written in hierarchical order

  module AKYCDIR
    @tempdirs = ["pages","pages/txt","pages/akyc","pages/akyc/images","pages/akyc/pdf","pages/akyc/pdf/separate", "pages/akyc/fonts"]
    @statdirs = ["data","data/csv"]
      def self.tempdirs
        @tempdirs
      end
      def self.statdirs
        @statdirs
      end
  end
