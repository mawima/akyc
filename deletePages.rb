#!/usr/bin/env ruby
require 'find'
require_relative 'whatDir'
require_relative 'whatFile'

  delroot = [AKYCDIR.tempdirs[0]]#,AKYCDIR.statdirs[0]]
  delroot.each{|i|
    if File.exists?(i)
    Find.find(i) { |d|
     if File.directory?(d)
        if d == AKYCDIR.tempdirs[6]
          puts "i do not delete folder fonts and its content" # if next line is Find.prune
          Find.prune
        end
     else File.file?(d)
       File.delete(d)
       puts "file: #{d} deleted"
    end
    }
  else
    puts "no files found, nothing to delete"
  end
  }
