#!/usr/bin/env ruby
# encoding: utf-8
require 'rasem'
require 'csv'
require_relative 'whatEnv'
require_relative 'whatDir'
require_relative 'whatFile'
require_relative 'whatComp'
require_relative 'makeDir'

  if !File.exists?(INfile)
    puts infile + " does not exist!"
    exit
  end

  data = CSV.foreach(INfile, headers:true, header_converters: :symbol, converters: :integer) {|r|

    height = FormHeight
    width = FormWidth
    points = []

    img = Rasem::SVGImage.new(:width => width, :height => height) do

      text(width,height,"font-family"=>"sans","font-size"=>(height*0.016).to_i,:fill=>"#e7e7e7"){raw "#{r[:pupilid]}-#{r[:date]}"}.rotate(-90,width,height)

      if r[:assayer].strip == "self"
        assayer = "Selbsteinschätzung"
        assessment = "Selbsteinschätzung durch #{r[:pupilid]}, #{r[:date]}"
      else
        assayer = r[:assayer]
        assessment = "Einschätzung von #{r[:pupilid]} durch die Klassenleitung #{r[:assayer]}, #{r[:date]}"
      end

      # copymark = r[:date] + ", " + r[:pupilid].upcase + ", CC-BY-SA 4.0, Norbert.Reschke@gMail.com"
      copymark = "CC-BY-SA 4.0, Norbert.Reschke@gMail.com"
      text(0,(height*0.999).to_i,"text-anchor"=>"left","font-family"=>"sans","font-size"=>(height*0.01).to_i){raw copymark}
      # text(0,0,"font-family"=>"sans","font-size"=>(height*0.03).to_i,:fill=>AsciiDocColor){raw r[:pupilid][0..7]}.rotate(4).translate(width*0.82,-height*0.027)
      text(0,(height*0.015).to_i,"text-anchor"=>"left","font-family"=>"sans","font-size"=>(height*0.01).to_i){raw assessment}

      line(width*0.749,(height*0.04).to_i,width*0.749,height.to_i)
      line(width*0.794,(height*0.04).to_i,width*0.794,height.to_i)
      line(width*0.838,(height*0.04).to_i,width*0.838,height.to_i)
      line(width*0.882,(height*0.04).to_i,width*0.882,height.to_i)
      line(width*0.926,(height*0.04).to_i,width*0.926,height.to_i)
      line(width*0.97,(height*0.04).to_i,width*0.97,height.to_i)

      def skillsline(height,width,hfac)
        line(width*0.72,(height*hfac).to_i,width,(height*hfac).to_i,:stroke=>"black")
        circle((width*0.749).to_i,(height*hfac).to_i,(height*0.012).to_i,:stroke=>"none",:fill=>"white")
        circle((width*0.794).to_i,(height*hfac).to_i,(height*0.012).to_i,:stroke=>"none",:fill=>"white")
        circle((width*0.838).to_i,(height*hfac).to_i,(height*0.012).to_i,:stroke=>"none",:fill=>"white")
        circle((width*0.882).to_i,(height*hfac).to_i,(height*0.012).to_i,:stroke=>"none",:fill=>"white")
        circle((width*0.926).to_i,(height*hfac).to_i,(height*0.012).to_i,:stroke=>"none",:fill=>"white")
        circle((width*0.97).to_i,(height*hfac).to_i,(height*0.012).to_i,:stroke=>"none",:fill=>"white")
      end

      skillsline(height,width,0.07) #row01
      skillsline(height,width,0.146)#row02
      skillsline(height,width,0.222)#row03
      skillsline(height,width,0.298)#row04
      skillsline(height,width,0.360)#row05
      skillsline(height,width,0.410)#row06
      skillsline(height,width,0.469)#row07
      skillsline(height,width,0.529)#row08
      skillsline(height,width,0.603)#row09
      skillsline(height,width,0.677)#row10
      skillsline(height,width,0.729)#row11
      skillsline(height,width,0.780)#row12
      skillsline(height,width,0.846)#row13
      skillsline(height,width,0.946)#row14

      cells = [r[:c01],r[:c02],r[:c03],r[:c04],r[:c05],r[:c06],r[:c07],r[:c08],r[:c09],r[:c10],r[:c11],r[:c12],r[:c13],r[:c14]]
      cells.each {|i|

        row = i[0, 2]
        col = i[2].upcase

        case col
          when "A"
            x = 0.749
          when "B"
            x = 0.794
          when "C"
            x = 0.838
          when "D"
            x = 0.882
          when "E"
            x = 0.926
          when "F"
            x = 0.97
        end

        case row
          when "01"
            y = 0.07
          when "02"
            y = 0.146
          when "03"
            y = 0.222
          when "04"
            y = 0.298
          when "05"
            y = 0.360
          when "06"
            y = 0.410
          when "07"
            y = 0.469
          when "08"
            y = 0.529
          when "09"
            y = 0.603
          when "10"
            y = 0.677
          when "11"
            y = 0.729
          when "12"
            y = 0.780
          when "13"
            y = 0.848
          when "14"
            y = 0.946
        end

        line((width*x-10).to_i,(height*y-10).to_i,(width*x+10).to_i,(height*y+10).to_i,:stroke=>"black")
        line((width*x+10).to_i,(height*y-10).to_i,(width*x-10).to_i,(height*y+10).to_i,:stroke=>"black")
        # circle((width*x).to_i,(height*y).to_i,(height*0.008).to_i,:stroke=>"none",:fill=>"white")
        # text((width*x).to_i,(height*(y+0.004)).to_i,"text-anchor"=>"middle","font-family"=>"sans",:fill=>"black","font-size"=>(height*0.01).to_i){raw col}

        points.push((height*x).to_i,(height*y).to_i)
      }

      #text rows
      # header
      text((width*0.003).to_i,(height*0.032).to_i,"font-family"=>"sans","font-size"=>(height*0.015).to_i){raw"Nr"}
      text((width*0.033).to_i,(height*0.032).to_i,"font-family"=>"sans","font-size"=>(height*0.015).to_i){raw "Kompetenz"}
      text((width*0.303).to_i,(height*0.032).to_i,"font-family"=>"sans","font-size"=>(height*0.015).to_i){raw "Erläuterung"}
      text((width*0.74).to_i,(height*0.03).to_i,"font-family"=>"sans","font-size"=>(height*0.023).to_i){raw "A"}
      text((width*0.784).to_i,(height*0.03).to_i,"font-family"=>"sans","font-size"=>(height*0.023).to_i){raw "B"}
      text((width*0.828).to_i,(height*0.03).to_i,"font-family"=>"sans","font-size"=>(height*0.023).to_i){raw "C"}
      text((width*0.872).to_i,(height*0.03).to_i,"font-family"=>"sans","font-size"=>(height*0.023).to_i){raw "D"}
      text((width*0.916).to_i,(height*0.03).to_i,"font-family"=>"sans","font-size"=>(height*0.023).to_i){raw "E"}
      text((width*0.96).to_i,(height*0.03).to_i,"font-family"=>"sans","font-size"=>(height*0.023).to_i){raw "F"}

      # A top to F worse picto
      text(0,0,"font-family"=>"sans","font-size"=>(height*0.008).to_i){raw "A = :), B = :("}.rotate(-90,10,10).translate(-height*0.025,width-width*0.002)
      #text((width*0.965).to_i,(height*0.008).to_i,"font-family"=>"sans","font-size"=>(height*0.008).to_i){raw "F  (:"}

      if r[:assayer].strip == "self"
        ass(width,height,0)
      elsif r[:sex] == "f"
        ass(width,height,1)
      elsif r[:sex] == "m"
        ass(width,height,2)
      else
        puts "something seems to be wrong with col assayer or col sex!"
      end

    end

    if r[:assayer].strip == "self"
      y = "pupil"
    else
      y = r[:assayer].strip.downcase
    end

    File.open(AKYCDIR.tempdirs[3] + "/#{r[:pupilid][0..7]}-#{r[:date]}-#{y}-form.svg", "w") { |f|
      img.write(f)
    }
  }
