#!/usr/bin/env ruby
# encoding: utf-8
require 'rasem'
# https://github.com/aseldawy/rasem
require_relative 'whatDir'
require_relative 'whatComp'

  def coloredLinks(filename)
  file = AKYCDIR.tempdirs[3] + "/" + filename + "-link.svg"
  img = Rasem::SVGImage.new(:width => 500, :height => 100) {

    # rectangle(  0, 0,500,50,:stroke=>"none",:fill=>"white") # background
    rectangle(100, 2, 20,98,:stroke=>"none",:fill=>CompColor["A"])
    rectangle(120, 2, 20,98,:stroke=>"none",:fill=>CompColor["B"])
    rectangle(140, 2, 20,98,:stroke=>"none",:fill=>CompColor["C"])
    rectangle(160, 2, 20,98,:stroke=>"none",:fill=>CompColor["D"])
    rectangle(180, 2, 20,98,:stroke=>"none",:fill=>CompColor["E"])
    rectangle(200, 2, 20,98,:stroke=>"none",:fill=>CompColor["F"])
    rectangle( 75,30,350,40,:stroke=>"none",:fill=>"lightgray")
    line(100,10,220,10,:stroke=>"white")
    line(100,20,220,20,:stroke=>"white")
    line(100,80,220,80,:stroke=>"white")
    line(100,90,220,90,:stroke=>"white")
    line(  0,50,500,50,:stroke=>"lightgray")
    circle(75,50,22,:stroke=>"lightgray",:stroke_width=>3,:fill=>"lightgray",:fill_opacity=>0.7)
    circle(425,50,20,:stroke=>"none",:fill=>"lightgray")
    text( 90,54,"font-family"=>"sans",:fill=>CompColor["A"],"font-size"=>26){raw filename[0,1]}
    text(112,52,"font-family"=>"sans",:fill=>CompColor["B"],"font-size"=>26){raw filename[1,1]}
    text(134,56,"font-family"=>"sans",:fill=>CompColor["C"],"font-size"=>26){raw filename[2,1]}
    text(156,53,"font-family"=>"sans",:fill=>CompColor["D"],"font-size"=>26){raw filename[3,1]}
    text(178,58,"font-family"=>"sans",:fill=>CompColor["E"],"font-size"=>26){raw filename[4,1]}
    text(200,55,"font-family"=>"sans",:fill=>CompColor["F"],"font-size"=>26){raw filename[5,1]}
    text(222,54,"font-family"=>"sans",:fill=>"#f8f8f8","font-size"=>25){raw filename[6,1]}
    text(244,58,"font-family"=>"sans",:fill=>"#e8e8e8","font-size"=>25){raw filename[7,1]}
    text(270,65,"font-family"=>"sans",:fill=>"white","font-size"=>22){raw filename[9,10]}
    text(300,95,"font-family"=>"sans",:fill=>"darkgray","font-size"=>20){raw "HTML-pages"}
  }
  
  File.open(file, "w") { |f|
    img.write(f)
  }

  end

  def grayLinks(filename)
  file = AKYCDIR.tempdirs[3] + "/" + filename + "-pdf-link.svg"
  img = Rasem::SVGImage.new(:width => 200, :height => 100, :opacity=>0.5) {

  # rectangle(0,0,200,100,:stroke=>"white",:fill=>"white") # background
    rectangle(48,2,98,98,:stroke=>"none",:fill=>"lightgray")
    line(5,50,195,50,:stroke=>"lightgray")
    rectangle(145,35,10,65,:stroke=>"none",:fill=>"gray")
    rectangle(145, 2,10, 5,:stroke=>"none",:fill=>"gray")
    text( 50, 18,"font-family"=>"sans",:fill=>"#a7a7a7","font-size"=>20){raw filename[0,1]}
    text( 64, 22,"font-family"=>"sans",:fill=>"white","font-size"=>20){raw filename[1,1]}
    text( 78, 20,"font-family"=>"sans",:fill=>"#a7a7a7","font-size"=>20){raw filename[2,1]}
    text( 89, 20,"font-family"=>"sans",:fill=>"white","font-size"=>20){raw filename[3,1]}
    text(102, 18,"font-family"=>"sans",:fill=>"#a7a7a7","font-size"=>20){raw filename[4,1]}
    text(115, 25,"font-family"=>"sans",:fill=>"white","font-size"=>20){raw filename[5,1]}
    text(128, 24,"font-family"=>"sans",:fill=>"#a7a7a7","font-size"=>20){raw filename[6,1]}
    text(142, 28,"font-family"=>"sans",:fill=>"#a7a7a7","font-size"=>20){raw filename[7,1]}
    text( 72, 46,"font-family"=>"sans",:fill=>"white","font-size"=>14){raw filename[9,10]}
    text( 48, 88,"font-family"=>"sans",:fill=>"white","font-size"=>50){raw "pdf"}
    text(117, 96,"font-family"=>"sans",:fill=>"white","font-size"=>11){raw "pages"}
    text(  0,100,"font-family"=>"sans",:fill=>"#7a7a7a","font-size"=>18){raw filename[0,8]}.rotate(-90).translate(-100,-55)
  }
  File.open(file, "w") { |f|
    img.write(f)
  }

  end
