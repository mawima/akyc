#!/usr/bin/env ruby
# encoding: utf-8
require 'asciidoctor'
require 'asciidoctor-pdf'
require_relative 'css'      # html-style, css
require_relative 'yml'      # pdf-style, yml
require_relative 'whatComp'
require_relative 'whatEnv'
require_relative 'links'
require_relative 'valCompName'
require_relative 'keyCompName'
require_relative 'uuid'
require_relative 'indexLinks.rb'
require_relative 'ak14'
require_relative 'form'
require 'hexapdf'

  img_v1 = []
  img_v2 = []
  img_v3 = []
  j = [img_v1,img_v2,img_v3]
    j.each_with_index {|img_v,i|
      Dir.glob(AKYCDIR.tempdirs[3] + "/*-20*-*-v" + (i+1).to_s + ".svg").each { |img|
        img_v.push(File.basename(img, ".svg"))
        }         # sample images 
      img_v.sort! #sort images
    }

  f = File.new(AKYCDIR.tempdirs[1] + "/index.txt", "w")
    f.write(":ext-relative: {outfilesuffix} \n")
    f.write(":notitle:\n")
    f.write(":nofooter:\n")
    f.write(":doctype: article \n")
    f.write(":doctitle: AKYC\n")
    f.write(":subject: AKYC\n")
    f.write(":keywords: JobMappeNRW, Selbsteinschätzung, Fremdbewertung, Ausbildungsvorbereitung, Nordrhein-Westfalen, Fachbereich Technik, Berufsfeld Bau- und Holztechnik \n")
    f.write(":copyright: CC-BY-SA 4.0 \n")
    f.write(" \n")
    f.write("== Ausbildungsvorbereitung \n")
    f.write("=== link:keycomp.html[Schlüsselkompetenzen] nach Jobmappe-NRW \n")
    f.write("==== You will find the source at link:https://bitbucket.org/mawima/AKYC[https://bitbucket.org/mawima/AKYC], Norbert Reschke, CC-BY-SA 4.0 \n")
    f.write(" \n")

  if img_v1.length < 1
    f.write("\n")
    f.write("Please run ruby ak14.rb before you run ruby makePages.rb.\n")
    f.write("\n")
    f.write("The webpages will be useless without content.\n")
    f.write("\n")
    f.write("Take a look at the README. \n")
    f.write("\n")
    f.write("To delete this page, just run ruby deletePages.rb.\n")
    f.write(" \n")

  else
    m = img_v1.length
    img_v1.each_with_index { |img,i|
      coloredLinks(img)
      grayLinks(img_v3[i])
      core = File.basename(img, "-v1")
      f.write(" \n")
      f.write("[.line] \n")
      f.write("CC-BY-SA 4.0, made with ruby -- especially with rasem (https://github.com/aseldawy/rasem), asciidoctor (https://github.com/asciidoctor/asciidoctor), hexapdf (https://github.com/gettalong/hexapdf) \n")
      f.write(" \n")
      f.write("[.floatleft] \n")
      f.write("image:" + img + "-link.svg[#{img},link=" + img +"{ext-relative}] \n")
      f.write(" \n")
      f.write("[.floatright] \n")
      f.write("image:" + img_v3[i] + "-pdf-link.svg[#{img_v3[i]},link=pdf/" + core + ".pdf] \n")  
      f.write(" \n")
    }

  end
      
  f.close

  Asciidoctor.render_file AKYCDIR.tempdirs[1] + "/index.txt",
    :base_dir => '.',
    :to_dir => AKYCDIR.tempdirs[2],
    :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images'

  pgs = [img_v1, img_v2]
  pgs.each_with_index { |p,j|
    p.each_with_index { |img,i|
      f = File.new(AKYCDIR.tempdirs[1] + "/" + img + ".txt", "w")
        f.write(":ext-relative: {outfilesuffix} \n")
        f.write(":notitle:\n")
        f.write(":nofooter:\n")
        f.write(":doctype: article \n")
        f.write(":doctitle: " + img + "\n")
        f.write(":copyright: CC-BY-SA 4.0 \n")
        f.write(" \n")
        f.write("[.center] \n") # maybe later a better idea
        f.write("[cols=\"1a,11,1a,10a\"] \n") # table, maybe later better idea with CSS only
        f.write("|=== \n")
        f.write("| \n")
        f.write("[cols=\"1\"] \n") # nested table
        f.write("!=== \n")
        f.write("! image:n0.svg[] \n") # dummy image, look at valCompName.rb

        CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
          case val.to_i
            when 1,7,8,9,10,11,13
              key = key.sub("ä","ae")
            end
 
          f.write("! image:ValCompName" + val + ".svg[link=" + key +"{ext-relative}]\n")
        }
    
        f.write("! image:n15.svg[] \n") # again dummy image, look at valCompName.rb
        f.write("!=== \n")
        f.write("|image:" + img + ".svg[bar,link=" + pgs[j-1][i] +"{ext-relative}] \n")
        f.write("| \n")
        f.write("[cols=\"1\"] \n") # again nested table
        f.write("!=== \n")
        f.write("! image:n0.svg[]  \n") 
 
        CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
          case val.to_i
            when 1,7,8,9,10,11,13
              key = key.sub("ä","ae")
            end

          f.write("! image:ValCompName" + val + ".svg[link=" + key +"{ext-relative}]\n")
        }
      
        f.write("! image:n15.svg[] \n")
        f.write("!=== \n")
        f.write("| \n")
        f.write("[cols=\"1\"] \n") # again nested table
        f.write("!=== \n")
        uuid(img)
        f.write("! image:" + img[0..7] + ".svg[] \n") # maybe here is a place for the uuid

        CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
          case val.to_i
            when 1,7,8,9,10,11,13
              key = key.sub("ä","ae")
            end

          f.write("! image:KeyCompName" + val + ".svg[link=" + key +"{ext-relative}]\n")
        }

        f.write("! image:n15.svg[] \n")
        f.write("!=== \n")
        f.write("|=== \n")
        f.write(" \n")
        f.write("[small]#record " + (i+1).to_s + " from " + m.to_s + "#\n")
        ci = File.basename(img_v3[i], "-v3")
        f.write("[topright]#image:" + img_v3[i] + "-pdf-link.svg[#{img_v3[i]},link=pdf/" + ci + ".pdf]# \n")
        f.write("[bottomleft]#image:back.svg[back, width=100%, height=100%, link=" + p[i-1] +"{ext-relative}]# \n")
        f.write("[bottomcenter]#image:home.svg[index, width=100%, height=100%, link=index{ext-relative}]# \n")
        f.write("[bottomright]#image:next.svg[next, width=100%, height=100%, link=" + p[i+1-m] +"{ext-relative}]# \n")
      f.close

    Asciidoctor.render_file AKYCDIR.tempdirs[1] + "/" + img + ".txt",
    :base_dir => '.',
    :to_dir => AKYCDIR.tempdirs[2],
    :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images'

    }
  }

  CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
    case val.to_i
      when 1,7,8,9,10,11,13
        key = key.sub("ä","ae")
      end

  f = File.new(AKYCDIR.tempdirs[1] + "/" + key + ".txt", "w")
    f.write(":ext-relative: {outfilesuffix} \n")
    f.write(":notitle:\n")
    f.write(":nofooter:\n")
    f.write(":doctype: article \n")
    f.write(":doctitle: " + key + "\n")
    f.write(":copyright: CC-BY-SA 4.0 \n")
    f.write(" \n")

    f.write("[.keyh1]#Ausbildungsvorbereitung# \n")
    f.write(" \n")
    f.write("[.keyh2]#Schlüsselkompetenzen nach Jobmappe-NRW# \n")
    f.write(" \n")
    f.write("[.keyline]#Norbert.Reschke@gMail.com, CC-BY-SA 4.0, made with https://www.ruby-lang.org[ruby] -- especially with https://github.com/aseldawy/rasem[rasem], https://github.com/asciidoctor/asciidoctor[asciidoctor], https://github.com/gettalong/hexapdf[hexapdf]# \n")
    f.write(" \n")

    f.write("[.topcell] \n")
    f.write("Schlüsselkompetenzen \n")
    f.write(" \n")
        
    m = CompetenceNames.length

    CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
      case val.to_i
        when 1,7,8,9,10,11,13
          key = key.sub("ä","ae")
        end

      f.write("[.keycell]#image:KeyCompName" + val + ".svg[link=" + key +"{ext-relative}]#\n")
      f.write(" \n")          
    }
    f.write(" \n")
      
    case val
      when "01"
        f.write("[.keyh3]#Zuverlässigkeit# \n")
        f.write(" \n")
        f.write("[.keycontent04]#" + CompetenceExplain[0][0][0] + "# \n")
        f.write(" \n")
        f.write("[.keycontent11]#" + CompetenceExplain[0][0][1] + "# \n")
        f.write(" \n")
        f.write("[.keycontent12]#" + CompetenceExplain[0][0][2] + "# \n")
        f.write(" \n")

      when "02"
        f.write("[.keyh3] \n")
        f.write("Lernbereit- \n")
        f.write("schaft \n")
        f.write(" \n")
        f.write("[.keycontent01]#Ich lerne gerne und bin neugierig, Neues zu lernen.# \n")
        f.write(" \n")
        f.write("[.keycontent02]#Ich bin bereit mehrere Lernwege auszuprobieren, um etwas zu lernen.# \n")
        f.write (" \n")
        
      when "03"
        f.write("[.keyh3] \n")
        f.write ("Leistungsbereit- \n")
        f.write("schaft \n")
        f.write(" \n")
        f.write("[.keycontent01]#Ich will meine Aufgaben bearbeiten.# \n")
        f.write("[.keycontent04]#Ich bearbeite meine Aufgaben zügig.# \n")
        f.write("[.keycontent14]#Ich strenge mich an, um meine Aufgaben zu erledigen.# \n")
        f.write(" \n")

      when "04"
        f.write("[.keyh3]#Ausdauer# \n")
        f.write(" \n")
        f.write("[.keycontent01]#Ich bringe meine Arbeit zu Ende, auch wenn es länger dauert.# \n")
        f.write("[.keycontent02]#Ich halte durch, auch wenn es anstrengend wird.# \n")
        f.write(" \n")

      when "05"
        f.write("[.keyh3]#Belastbarkeit# \n")
        f.write(" \n")
        f.write("[.keycontent04]#" + CompetenceExplain[4][0][0] + "# \n")
        f.write(" \n")

      when "06"

        f.write("[.keyh3]#Sorgfalt# \n")
        f.write(" \n")
        f.write("[.keycontent01]#Ich arbeite sauber und ordentlich.# \n")
        f.write(" \n")
        f.write("[.keycontent04]#Ich versuche genau nach Plan zu arbeiten.# \n")
        f.write(" \n")
        
      when "07"

        f.write("[.keyh3] \n")
        f.write("Konzentrations-fähigkeit \n")
        f.write(" \n")
        f.write("[.keycontent01]#Ich bleibe bei meiner Aufgabe und lasse mich nicht ablenken.# \n")
        f.write(" \n")

      when "08"
        f.write("[.keyh3]#Selbstständig-keit# \n")
        f.write(" \n")
        f.write("[.keycontent08]#Ich kann eigenständig handeln und Verantwortung übernehmen.# \n")
        f.write(" \n")
        
      when "09"
        f.write("[.keyh3]#Kritikfähigkeit# \n")
        f.write(" \n")
        f.write("[.keycontent06]#Ich kann Fehler bei der Arbeit von anderen erkennen und ihnen helfen, die Fehler zu verbessern.# \n")
        f.write(" \n")
        f.write("[.keycontent07]#Wenn andere meine Fehler verbessern, lasse ich mir helfen.# \n")
        f.write(" \n")

      when "10"
        f.write("[.keyh3]#Kreativität# \n")
        f.write(" \n")
        f.write("[.keycontent08]#" + CompetenceExplain[9][0][0] + "# \n")
        f.write(" \n")
        f.write("[.keycontent12]#" + CompetenceExplain[9][0][1] + "# \n")
        f.write(" \n")
        
      when "11"
        f.write("[.keyh3]#Teamfähigkeit# \n")
        f.write(" \n")
        f.write("[.keycontent06]#" + CompetenceExplain[10][0][0] + "# \n")
        f.write(" \n")
        
      when "12"
        f.write("[.keyh3]#Umgangs-formen# \n")
        f.write(" \n")
        f.write("[.keycontent10]#" + CompetenceExplain[11][0][0] + "# \n")
        f.write("[.keycontent12]#" + CompetenceExplain[11][0][1] + "# \n")
        f.write(" \n")
        
      when "13"
        f.write("[.keyh3]#Konflikt-fähigkeit# \n")
        f.write(" \n")
        f.write("[.keycontent12]#Probleme mit anderen kann ich lösen.# \n")
        f.write(" \n")
        f.write("[.keycontent08]#Ich lasse mich nicht aus der Ruhe bringen, wenn es stressig wird.# \n")
        f.write(" \n")
        
      when "14"
        f.write("[.keyh3]#Toleranz# \n")
        f.write(" \n")
        f.write("[.keycontent02]#Ich halte es aus, wenn Menschen nicht meiner Meinung sind.# \n")
        f.write(" \n")
        f.write("[.keycontent04]#Ich kann es aushalten, wenn Menschen anders aussehen als ich.# \n")
        f.write(" \n")
        f.write("[.keycontent07]#Es ist kein Problem, wenn jemand eine andere Religion hat.# \n")
        f.write(" \n")
        f.write("[.keycontent08]#Es ist kein Problem, wenn jemand aus einem anderen Land kommt.# \n")
        f.write(" \n")
    end
        
    f.write("[bottomleft]#image:back.svg[back, width=100%, height=100%, link=" + CompetenceNames.values[val.to_i-2].to_s.sub("ä","ae") + "{ext-relative}]# \n")
    f.write("[bottomcenter]#image:home.svg[index, width=100%, height=100%, link=index{ext-relative}]# \n")
    f.write("[bottomright]#image:next.svg[next, width=100%, height=100%, link=" + CompetenceNames.values[val.to_i-m].to_s.sub("ä","ae") + "{ext-relative}]# \n")
  f.close

  Asciidoctor.render_file AKYCDIR.tempdirs[1] + "/" + key + ".txt",
    :base_dir => '.',
    :to_dir => AKYCDIR.tempdirs[2],
    :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images'
  }

  f = File.new(AKYCDIR.tempdirs[1] + "/keycomp.txt", "w")
    f.write(":ext-relative: {outfilesuffix} \n")
    f.write(":notitle:\n")
    f.write(":nofooter:\n")
    f.write(":doctype: article \n")
    f.write(":doctitle: Schlüsselkompetenzen \n")
    f.write(":copyright: CC-BY-SA 4.0 \n")
    f.write(" \n")

    f.write("[.keyh1]#Ausbildungsvorbereitung# \n")
    f.write(" \n")
    f.write("[.keyh2]#Schlüsselkompetenzen nach Jobmappe-NRW# \n")
    f.write(" \n")
    f.write("[.keyline]#Norbert.Reschke@gMail.com, CC-BY-SA 4.0, made with https://www.ruby-lang.org[ruby] -- especially with https://github.com/aseldawy/rasem[rasem], https://github.com/asciidoctor/asciidoctor[asciidoctor], https://github.com/gettalong/hexapdf[hexapdf]# \n")
    f.write(" \n")

    f.write("[.topcell] \n")
    f.write("Schlüsselkompetenzen \n")
    f.write(" \n")
        

  m = CompetenceNames.length
  CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
    case val.to_i
      when 1,7,8,9,10,11,13
        key = key.sub("ä","ae")
      end

    f.write("[.keycell]#image:KeyCompName" + val + ".svg[link=" + key +"{ext-relative}]#\n")
    f.write(" \n")          
  }
    f.write(" \n")
    f.write("[.keyh3] \n")
    f.write("Schlüssel- \n")
    f.write("kompetenzen \n")
    f.write(" \n")

  m = CompetenceNames.length
  CompetenceNames.each {|(val,key)| # images of numbers 1 to 14, look at valCompName.rb
    f.write("[.keytext" + val + "]#" + key +"#\n")
    f.write(" \n")
  }
    f.write(" \n")
    f.write("[bottomcenter]#image:home.svg[index, width=100%, height=100%, link=index{ext-relative}]# \n")
  f.close

  Asciidoctor.render_file AKYCDIR.tempdirs[1] + "/keycomp.txt",
    :base_dir => '.',
    :to_dir => AKYCDIR.tempdirs[2],
    :attributes => 'linkcss stylesdir=. stylesheet=adoc.css imagesdir=images'


  img_v3.each_with_index { |img,i|
    f = File.new(AKYCDIR.tempdirs[1] + "/" + img + ".txt", "w")
      f.write(":ext-relative: {outfilesuffix} \n")
      f.write(":notitle:\n")
      f.write(":nofooter:\n")
      f.write(":doctype: article \n")
      f.write(":doctitle: " + img + "\n")
      f.write(":copyright: CC-BY-SA 4.0 \n")
      f.write(" \n")
      f.write(":pdf-page-layout: landscape \n")
      f.write("image::" + img + ".svg[]\n")
      f.write(" \n")
    f.close


  Asciidoctor.render_file AKYCDIR.tempdirs[1] + "/#{img}.txt",
    :base_dir => '.',
    :to_dir => AKYCDIR.tempdirs[5],
    :attributes => 'pdf-stylesdir=' + AKYCDIR.tempdirs[2] + ' pdf-style=akyc  imagesdir=' + AKYCDIR.tempdirs[3] + ' pdf-fontsdir=' + AKYCDIR.tempdirs[6], # if not exist akyc-yml is make with yml.rb
    :backend => 'pdf'
  }


  img_v3.each_with_index { |img,i|
    core = File.basename(img, "-v3")

    f = File.new(AKYCDIR.tempdirs[1] + "/" + core + "-form.txt", "w")

      f.write(":ext-relative: {outfilesuffix} \n")
      f.write(":notitle:\n")
      f.write(":nofooter:\n")
      f.write(":doctype: article \n")
      f.write(":doctitle:  form \n")
      f.write(":copyright: CC-BY-SA 4.0 \n")
      f.write(" \n")
      f.write("image::" + core + "-form.svg[pdfwidth\=200mm]\n")
      f.write(" \n")
    f.close

  Asciidoctor.render_file AKYCDIR.tempdirs[1] + "/" + core + "-form.txt",
    :base_dir => '.',
    :to_dir => AKYCDIR.tempdirs[5],
    :attributes => 'pdf-stylesdir=' + AKYCDIR.tempdirs[2] + ' pdf-style=akyc  imagesdir=' + AKYCDIR.tempdirs[3] + ' pdf-fontsdir=' + AKYCDIR.tempdirs[6], # if not exist akyc-yml is make with yml.rb
    :backend => 'pdf'
  }

  # merge form and v3 images to pupils handout
  img_v3.each { |img|

    target = HexaPDF::Document.new
    core = File.basename(img, "-v3")

    pdf = HexaPDF::Document.open(AKYCDIR.tempdirs[5] + "/" + core + "-form.pdf")
    pdf.pages.each {|p|
      target.pages << target.import(p)
    }

    pdf = HexaPDF::Document.open(AKYCDIR.tempdirs[5] + "/" + core + "-v3.pdf")
    pdf.pages.each {|p|
      target.pages << target.import(p)
    }

  target.write(AKYCDIR.tempdirs[4] + "/" + core + ".pdf",optimize:true)
  }
