#!/usr/bin/env ruby
# encoding: utf-8
require_relative 'whatDir'
require_relative 'whatComp'
require 'rasem'
# https://github.com/aseldawy/rasem
# designations for key competences (only web-pages)

  height = HEIGHT  # change HEIGHT in whatComp.rb
  width = height/2 # col width for nested tables second col
  y14 = height*0.9 # height over all, for nested tables 14 numbered rows 2 to 15
  y = y14/14       # height of one row (nested tables rows 2 to 15)

  CompetenceNames.each {|(val,key)|  # designations for key competences
    case val.to_i
      when  1
        w = width-width/1.9
      when  2
        w = width-width/2
      when  3
        w = width-width/3
      when  4
        w = width-width/1.45
      when  5
        w = width-width/1.7
      when  6
        w = width-width/1.35
      when  7
        w = width-width/3.7
      when  8
        w = width-width/2.2
      when  9
        w = width-width/1.8
      when 10
        w = width-width/1.5
      when 11
        w = width-width/1.8
      when 12
        w = width-width/2.1
      when 13
        w = width-width/2
      when 14
        w = width-width/1.35
    end

  file = AKYCDIR.tempdirs[3] + "/KeyCompName" + val.to_s + ".svg"
  img = Rasem::SVGImage.new(:width => width, :height => y) {
    text(height/160,y/2,fill:"gray","dominant-baseline"=>"central","text-anchor"=>"start","font-family"=>"sans","font-size"=>y*0.45){raw key}
    line(w,y/2,width,y/2,:stroke=>"lightgray",:stroke_width=>(height*0.003).to_i)
  }

  if !File.exists?(file)
    File.open(file, "w") { |f|
      img.write(f)
    }
  end
  }
