#!/usr/bin/env ruby
# encoding: utf-8
require_relative 'whatFile'

  HEIGHT = 800
  FormHeight = 1024
  FormWidth = (FormHeight*(210.0/297.0)).to_i

  AsciiDocColor = "#527bbd"
  CompColor = {"A"=>"#c4ff4e","B"=>"#b8ff5a","C"=>"#acff4e","D"=>"#a0ff42","E"=>"#ffaa1e","F"=>"#ff1f00"}
  CircColor = {"A"=>"green","B"=>"green","C"=>"green","D"=>"green","E"=>"#ff110f","F"=>"#a50813"}

  CompetenceNames = {"01"=>"Zuverlässigkeit",
                     "02"=>"Lernbereitschaft",
                     "03"=>"Leistungsbereitschaft",
                     "04"=>"Ausdauer",
                     "05"=>"Belastbarkeit",
                     "06"=>"Sorgfalt",
                     "07"=>"Konzentrationsfähigkeit",
                     "08"=>"Selbstständigkeit",
                     "09"=>"Kritikfähigkeit",
                     "10"=>"Kreativität",
                     "11"=>"Teamfähigkeit",
                     "12"=>"Umgangsformen",
                     "13"=>"Konfliktfähigkeit",
                     "14"=>"Toleranz"}

  CompetenceExplain = [["Ich halte mich an Regeln und Absprachen.",
                         "Ich stehe zu meinem Wort.",
                         "Ich erledige meine Aufgaben."],
                        ["Die Schülerin hält sich an Regeln und Absprachen.",
                         "Sie steht zu ihrem Wort.",
                         "Sie erledigt ihre Aufgaben."],
                        ["Der Schüler hält sich an Regeln und Absprachen.",
                         "Er steht zu seinem Wort.",
                         "Er erledigt seine Aufgaben."]],
                       # 02 
                       [["Ich lerne gerne und ich bin neugierig, Neues",
                         "zu lernen. Ich bin bereit mehrere Lernwege",
                         "auszuprobieren, um etwas zu lernen."],
                        ["Die Schülerin lernt gerne und ist neugierig,",
                         "Neues zu lernen. Sie ist bereit mehrere Lern-",
                         "wege auszuprobieren, um etwas zu lernen."],
                        ["Der Schüler lernt gerne und ist neugierig,",
                         "Neues zu lernen. Er ist bereit mehrere Lern-",
                         "wege auszuprobieren, um etwas zu lernen."]],
                        # 03
                        [["Ich habe den festen Willen meine Aufgaben zu be-",
                         "arbeiten. Ich bearbeite meine Aufgaben zügig. Ich",
                         "strenge mich an, um meine Aufgaben zu erledigen."],
                         ["Die Schülerin will ihre Aufgaben bearbeiten.",
                          "Sie bearbeitet ihre Aufgaben zügig.",
                          "Sie strengt sich an, um ihre Aufgaben zu erledigen."],
                         ["Der Schüler will seine Aufgaben bearbeiten.",
                          "Er bearbeitet seine Aufgaben zügig.",
                          "Er strengt sich an, um seine Aufgaben zu erledigen."]],
                        # 04  
                        [["Ich bringe meine Arbeit zu Ende, auch wenn es",
                          "länger dauert. Ich halte durch, auch wenn es an-",
                          "strengend wird."],
                          ["Die Schülerin bringt ihre Arbeit zu Ende, auch",
                           "wenn es länger dauert. Sie hält durch, auch",
                           "wenn es anstrengend wird."],
                          ["Der Schüler bringt seine Arbeit zu Ende, auch",
                           "wenn es länger dauert. Er hält durch, auch",
                           "wenn es anstrengend wird."]],
                         # 05
                         [["Ich halte Stress aus."],
                          ["Die Schülerin hält Stress aus."],
                          ["Der Schüler hält Stress aus."]],
                         # 06
                         [["Ich arbeite sauber und ordentlich. Ich versuche",
                           "genau nach Plan zu arbeiten."],
                          ["Die Schülerin arbeitet sauber und ordentlich.",
                           "Sie versucht genau nach Plan zu arbeiten."],
                          ["Der Schüler arbeitet sauber und ordentlich.",
                           "Er versucht genau nach Plan zu arbeiten."]],
                         # 07
                         [["Ich bleibe bei meiner Aufgabe und lasse mich",
                           "nicht ablenken."],
                          ["Die Schülerin bleibt bei ihrer Aufgabe und lässt",
                           "sich nicht ablenken."],
                          ["Der Schüler bleibt bei seiner Aufgabe und lässt",
                           "sich nicht ablenken."]],
                         # 08
                         [["Ich kann eigenständig handeln und Verantwortung",
                           "übernehmen."],
                          ["Die Schülerin kann eigenständig handeln und",
                           "Verantwortung übernehmen."],
                          ["Der Schüler kann eigenständig handeln und",
                           "Verantwortung übernehmen."]],
                         # 09
                         [["Ich kann Fehler bei der Arbeit von anderen er-",
                           "kennen und ihnen helfen, die Fehler zu verbes-",
                           "sern. Ich lasse mir helfen, wenn andere meine",
                           "Fehler verbessern."],
                          ["Die Schülerin kann Fehler bei der Arbeit von anderen",
                           "erkennen und ihnen helfen, die Fehler zu ver-",
                           "bessern. Sie lässt sich helfen, wenn andere ihre",
                           "Fehler verbessern."],
                          ["Der Schüler kann Fehler bei der Arbeit von anderen",
                           "erkennen und ihnen helfen, die Fehler zu ver-",
                           "bessern. Er lässt sich helfen, wenn andere seine",
                           "Fehler verbessern."]],
                         # 10
                         [["Ich habe eigene Ideen.",
                           "Mir fällt immer etwas ein."],
                          ["Die Schülerin hat eigene Ideen.",
                           "Ihr fällt immer etwas ein."],
                          ["Der Schüler hat eigene Ideen.",
                          "Ihm fällt immer etwas ein."]],
                         # 11
                         [["Ich kann mit anderen zusammenarbeiten."],
                          ["Die Schülerin kann mit anderen zusammenarbeiten."],
                          ["Der Schüler kann mit anderen zusammenarbeiten."]],
                         # 12
                         [["Ich bin höflich und freundlich.",
                           "Ich helfe anderen."],
                          ["Die Schülerin ist höflich und freundlich.",
                           "Sie hilft anderen."],
                          ["Der Schüler ist höflich und freundlich.",
                           "Er hilft anderen."]],
                         # 13
                         [["Probleme mit anderen kann ich lösen. Ich lasse",
                           "mich nicht aus der Ruhe bringen, wenn es",
                           "stressig wird."],
                          ["Probleme mit anderen kann die Schülerin lösen.",
                           "Sie lässt sich nicht aus der Ruhe bringen,",
                           "wenn es stressig wird."],
                          ["Probleme mit anderen kann der Schüler lösen.",
                           "Er lässt sich nicht aus der Ruhe bringen,",
                           "wenn es stressig wird."]],
                         # 14
                         [["Ich halte es aus, wenn Menschen nicht meiner",
                           "Meinung sind. Ich kann es aushalten, wenn",
                           "Menschen anders aussehen als ich. Mir macht",
                           "es kein Problem, wenn jemand eine andere",
                           "Religion hat. Für mich ist kein Problem, wenn",
                           "jemand aus einem anderen Land kommt."],
                          ["Die Schülerin hält es aus, wenn Menschen nicht",
                           "ihrer Meinung sind. Sie kann es aushalten,",
                           "wenn Menschen anders aussehen als Sie.",
                           "Es ist kein Problem, wenn jemand eine andere",
                           "Religion hat. Es ist kein Problem, wenn jemand",
                           "aus einem anderen Land kommt."],
                          ["Die Schülerin hält es aus, wenn Menschen nicht",
                           "ihrer Meinung sind. Sie kann es aushalten,",
                           "wenn Menschen anders aussehen als Sie.",
                           "Es ist kein Problem, wenn jemand eine andere",
                           "Religion hat. Es ist kein Problem, wenn jemand",
                           "aus einem anderen Land kommt."]]

  def ass(width,height,who)
    #row 1
    text((width*0.003).to_i,(height*0.07).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[0]}.translate(0,height*0.005*1)
    text((width*0.033).to_i,(height*0.07).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[0]}.translate(0,height*0.005*1)
    text((width*0.303).to_i,(height*0.054).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[0][who][0]}.translate(0,height*0.005*1)
    text((width*0.303).to_i,(height*0.07).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[0][who][1]}.translate(0,height*0.005*1)
    text((width*0.303).to_i,(height*0.086).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[0][who][2]}.translate(0,height*0.005*1)

    #row 2
    text((width*0.003).to_i,(height*0.14).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[1]}.translate(0,height*0.005*2)
    text((width*0.033).to_i,(height*0.14).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[1]}.translate(0,height*0.005*2)
    text((width*0.303).to_i,(height*0.124).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[1][who][0]}.translate(0,height*0.005*2)
    text((width*0.303).to_i,(height*0.14).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[1][who][1]}.translate(0,height*0.005*2)
    text((width*0.303).to_i,(height*0.156).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[1][who][2]}.translate(0,height*0.005*2)

    #row 3
    text((width*0.003).to_i,(height*0.21).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[2]}.translate(0,height*0.005*3)
    text((width*0.033).to_i,(height*0.21).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[2]}.translate(0,height*0.005*3)
    text((width*0.303).to_i,(height*0.194).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[2][who][0]}.translate(0,height*0.005*3)
    text((width*0.303).to_i,(height*0.21).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[2][who][1]}.translate(0,height*0.005*3)
    text((width*0.303).to_i,(height*0.226).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[2][who][2]}.translate(0,height*0.005*3)

    #row 4
    text((width*0.003).to_i,(height*0.284).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[3]}.translate(0,height*0.005*4)
    text((width*0.033).to_i,(height*0.284).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[3]}.translate(0,height*0.005*4)
    text((width*0.303).to_i,(height*0.268).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[3][who][0]}.translate(0,height*0.005*4)
    text((width*0.303).to_i,(height*0.284).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[3][who][1]}.translate(0,height*0.005*4)
    text((width*0.303).to_i,(height*0.3).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[3][0][who]}.translate(0,height*0.005*4)

    #row 5
    text((width*0.003).to_i,(height*0.338).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[4]}.translate(0,height*0.005*5)
    text((width*0.033).to_i,(height*0.338).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[4]}.translate(0,height*0.005*5)
    text((width*0.303).to_i,(height*0.338).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[4][who][0]}.translate(0,height*0.005*5)

    #row 6
    text((width*0.003).to_i,(height*0.384).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[5]}.translate(0,height*0.005*6)
    text((width*0.033).to_i,(height*0.384).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[5]}.translate(0,height*0.005*6)
    text((width*0.303).to_i,(height*0.376).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[5][who][0]}.translate(0,height*0.005*6)
    text((width*0.303).to_i,(height*0.392).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[5][who][1]}.translate(0,height*0.005*6)

    #row 7
    text((width*0.003).to_i,(height*0.438).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[6]}.translate(0,height*0.005*7)
    text((width*0.033).to_i,(height*0.438).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[6]}.translate(0,height*0.005*7)
    text((width*0.303).to_i,(height*0.43).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[6][who][0]}.translate(0,height*0.005*7)
    text((width*0.303).to_i,(height*0.446).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[6][who][1]}.translate(0,height*0.005*7)

    #row 8
    text((width*0.003).to_i,(height*0.492).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[7]}.translate(0,height*0.005*8)
    text((width*0.033).to_i,(height*0.492).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[7]}.translate(0,height*0.005*8)
    text((width*0.303).to_i,(height*0.484).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[7][who][0]}.translate(0,height*0.005*8)
    text((width*0.303).to_i,(height*0.5).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[7][who][1]}.translate(0,height*0.005*8)

    #row 9
    text((width*0.003).to_i,(height*0.562).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[8]}.translate(0,height*0.005*9)
    text((width*0.033).to_i,(height*0.562).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[8]}.translate(0,height*0.005*9)
    text((width*0.303).to_i,(height*0.538).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[8][who][0]}.translate(0,height*0.005*9)
    text((width*0.303).to_i,(height*0.554).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[8][who][1]}.translate(0,height*0.005*9)
    text((width*0.303).to_i,(height*0.57).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[8][who][2]}.translate(0,height*0.005*9)
    text((width*0.303).to_i,(height*0.586).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[8][who][3]}.translate(0,height*0.005*9)

    #row 10
    text((width*0.003).to_i,(height*0.632).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[9]}.translate(0,height*0.005*10)
    text((width*0.033).to_i,(height*0.632).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[9]}.translate(0,height*0.005*10)
    text((width*0.303).to_i,(height*0.624).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[9][who][0]}.translate(0,height*0.005*10)
    text((width*0.303).to_i,(height*0.64).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[9][who][1]}.translate(0,height*0.005*10)

    #row 11
    text((width*0.003).to_i,(height*0.678).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[10]}.translate(0,height*0.005*11)
    text((width*0.033).to_i,(height*0.678).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[10]}.translate(0,height*0.005*11)
    text((width*0.303).to_i,(height*0.678).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[10][who][0]}.translate(0,height*0.005*11)

    #row 12
    text((width*0.003).to_i,(height*0.724).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[11]}.translate(0,height*0.005*12)
    text((width*0.033).to_i,(height*0.724).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[11]}.translate(0,height*0.005*12)
    text((width*0.303).to_i,(height*0.716).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[11][who][0]}.translate(0,height*0.005*12)
    text((width*0.303).to_i,(height*0.732).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[11][who][1]}.translate(0,height*0.005*12)

    #row 13
    text((width*0.003).to_i,(height*0.786).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[12]}.translate(0,height*0.005*13)
    text((width*0.033).to_i,(height*0.786).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[12]}.translate(0,height*0.005*13)
    text((width*0.303).to_i,(height*0.77).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[12][who][0]}.translate(0,height*0.005*13)
    text((width*0.303).to_i,(height*0.786).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[12][who][1]}.translate(0,height*0.005*13)
    text((width*0.303).to_i,(height*0.802).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[12][who][2]}.translate(0,height*0.005*13)

    #row 14
    text((width*0.003).to_i,(height*0.88).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceNames.keys[13]}.translate(0,height*0.005*14)
    text((width*0.033).to_i,(height*0.88).to_i,"font-family"=>"sans","font-size"=>(height*0.018).to_i){raw CompetenceNames.values[13]}.translate(0,height*0.005*14)
    text((width*0.303).to_i,(height*0.84).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[13][who][0]}.translate(0,height*0.005*14)
    text((width*0.303).to_i,(height*0.856).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[13][who][1]}.translate(0,height*0.005*14)
    text((width*0.303).to_i,(height*0.872).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[13][who][2]}.translate(0,height*0.005*14)
    text((width*0.303).to_i,(height*0.888).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[13][who][3]}.translate(0,height*0.005*14)
    text((width*0.303).to_i,(height*0.904).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[13][who][4]}.translate(0,height*0.005*14)
    text((width*0.303).to_i,(height*0.92).to_i,"font-family"=>"sans","font-size"=>(height*0.013).to_i){raw CompetenceExplain[13][who][5]}.translate(0,height*0.005*14)
  end
