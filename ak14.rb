#!/usr/bin/env ruby
# encoding: utf-8
require 'rasem'
require 'csv'
require_relative 'whatEnv'
require_relative 'whatDir'
require_relative 'whatFile'
require_relative 'whatComp'
require_relative 'makeDir'


  if !File.exists?(INfile)
    puts INFile + " does not exist!"
    exit
  end

  height = HEIGHT # change HEIGHT in whatComp.rb
  colwd  = 0.14   # multiplier to get col width in relation to height

  data = CSV.foreach(INfile, headers:true, header_converters: :symbol, converters: :numeric) {|r|

    (1..3).each { |z|
      if z != 3
        width = (height*colwd*6).to_i # only 6 rows will be drawn
      else
        width = (height*1.41).to_i
      end

      points = []     # array for polyline-points (circle to circle)
      bar    = 0      # value of x-position of bar (avg comp values)

      img = Rasem::SVGImage.new(:width => width, :height => height) do
        pos=0
        if z != 3 # draw rows for web-pages, background
          CompColor.each {|key,val|
            rectangle((height*pos).to_i,(height*0.02).to_i,(height*colwd).to_i,(height*0.96).to_i,:stroke=>"none",:fill=>"#{val}")
            text((height*(colwd+pos-0.08)).to_i,(height*0.06).to_i,"font-family"=>"sans",:fill=>"white","font-size"=>(height*0.034).to_i){raw "#{key}"}
            pos=pos+colwd
          }
        else # draw rows for pdf-pages, background
          CompColor.each {|i|
            line((height*(colwd+pos)).to_i,50,(height*(colwd+pos)).to_i,height-15,:stroke=>"lightgray",:stroke_width=>(height*0.003).to_i)
            pos=pos+colwd
          }
        end

        if z != 3 # draw center line in rows for web-pages
          pos = 0
          CompetenceNames.each {|i|
            line(0,(height*(1.51+pos)/14).to_i,width,(height*(1.51+pos)/14).to_i,:stroke=>"white",:stroke_width=>(height*0.003).to_i,:stroke_opacity=>1.0)
            pos=pos+0.9
          }
        elsif z == 3 # draw center line and designation for rows for pdf-pages,
          pos = 0
          col = "lightgray"
          CompetenceNames.each {|key,val|
            line(0,(height*(1.51+pos)/14).to_i,width,(height*(1.51+pos)/14).to_i,:stroke=>"lightgray",:stroke_width=>(height*0.003).to_i,:stroke_opacity=>1.0)
            text((height*0.02).to_i,(height*(1.75+pos)/14).to_i,"font-family"=>"sans","font-size"=>(height*0.038).to_i){raw "#{key}"}
            text((height*0.93).to_i,(height*(1.75+pos)/14).to_i,"font-family"=>"sans","font-size"=>(height*0.038).to_i){raw "#{val}"}
            pos=pos+0.9
          }
        else
          puts "something really went wrong"
          puts "var z has to be 1,2 or 3"
        end

        # data
        cells = [r[:c01],r[:c02],r[:c03],r[:c04],r[:c05],r[:c06],r[:c07],r[:c08],r[:c09],r[:c10],r[:c11],r[:c12],r[:c13],r[:c14]]
        cells.each {|i|
          row = i[0, 2]
          col = i[2].upcase

          case col
            when "A"
              x = 0.5*colwd
            when "B"
              x = 1.5*colwd
            when "C"
              x = 2.5*colwd
            when "D"
              x = 3.5*colwd
            when "E"
              x = 4.5*colwd
            when "F"
              x = 5.5*colwd
          end

          case row
            when "01"
              y = 0.11
            when "02"
              y = 0.174
            when "03"
              y = 0.238
            when "04"
              y = 0.302
            when "05"
              y = 0.366
            when "06"
              y = 0.43
            when "07"
              y = 0.494
            when "08"
              y = 0.558
            when "09"
              y = 0.622
            when "10"
              y = 0.686
            when "11"
              y = 0.75
            when "12"
              y = 0.814
            when "13"
              y = 0.878
            when "14"
              y = 0.942
          end

          bar = bar + x
  
          g = def_group("compcirc#{row}") do # def show data (value and term)
            if z != 3
              circle((height*x).to_i,(height*y).to_i, (height*0.029).to_i,:stroke=>"white",:stroke_width=>(height*0.006).to_i,:fill=>CircColor[col],:fill_opacity=>0.7,:stroke_opacity=>1.0)
              text((height*x).to_i,(height*y).to_i,"dominant-baseline"=>"central","text-anchor"=>"middle","font-family"=>"sans",:fill=>"white","font-size"=>(height*0.025).to_i){raw col}
            else
              x = x + colwd/2
              circle((height*x).to_i,(height*y).to_i, (height*0.027).to_i,:stroke=>"black",:stroke_width=>(height*0.003).to_i,:fill=>"white")
              text((height*x).to_i,(height*y+12).to_i,"text-anchor"=>"middle","font-family"=>"sans",:fill=>"black","font-size"=>(height*0.04).to_i){raw col}
            end
          end

          points.push((height*x).to_i,(height*y).to_i) #  draw data (value and term)

        }

        g = def_group("bar") do
          x = bar/14
          if z != 3
            line((height*x).to_i,(height*0.07).to_i,(height*x).to_i,height-5,:stroke=>"white",:stroke_width=>(height*colwd).to_i,:stroke_opacity=>0.7)                                # white background
            line((height*(x-colwd/2)).to_i,(height*0.022).to_i,(height*(x+colwd/2)).to_i,(height*0.022).to_i,:stroke=>"gray",:stroke_width=>(height*0.004).to_i,:stroke_opacity=>0.4) # first top border line
            line((height*(x-colwd/2)).to_i,(height*0.07).to_i,(height*(x+colwd/2)).to_i,(height*0.07).to_i,:stroke=>"gray",:stroke_width=>(height*0.004).to_i,:stroke_opacity=>0.4)   # second top border line
            line((height*(x-colwd/2)).to_i,(height*0.02).to_i,(height*(x-colwd/2)).to_i,(height*0.98),:stroke=>"gray",:stroke_width=>(height*0.005).to_i,:stroke_opacity=>0.4)        # left border line
            line((height*(x+colwd/2)).to_i,(height*0.02).to_i,(height*(x+colwd/2)).to_i,(height*0.98),:stroke=>"gray",:stroke_width=>(height*0.005).to_i,:stroke_opacity=>0.4)        # right border line
            line((height*(x-colwd/2)).to_i,(height*0.978).to_i,(height*(x+colwd/2)).to_i,(height*0.978),:stroke=>"gray",:stroke_width=>(height*0.004).to_i,:stroke_opacity=>0.4)      # first bottom border line
          else
            x = x + colwd/2
            line((height*x).to_i,50,(height*x).to_i,height-15,:stroke=>"lightgray",:stroke_width=>(height*colwd).to_i,:stroke_opacity=>0.5)
          end
        end

        if z != 1
          use("bar")
        end

        def compLine(col,points,height)
          polyline(:points=>points,:stroke=>col,:stroke_width=>(height*0.008).to_i,:fill=>"none") # polyline circle to circle
        end
  
        if z != 3
          compLine("white",points,height)
        else
          compLine("black",points,height/2)
        end
  
        (01..14).each{|i|
          v = i.to_s.rjust(2,'0')
          use("compcirc#{v}")
        } # do not know howto group add, so just made 14 groups only to draw circle over polyline

        copymark = r[:date] + ", " + r[:pupilid].upcase + ", CC-BY-SA 4.0, Norbert.Reschke@gMail.com"
        if r[:assayer].strip == "self"
          assayer = "Selbsteinschätzung"
          if z != 3
            assessment = "Selbsteinschätzung durch #{r[:pupilid]}"
            text((width/2).to_i,(height*0.995).to_i,:fill=>AsciiDocColor,"text-anchor"=>"middle","font-family"=>"sans","font-size"=>(height*0.017).to_i){raw copymark}
            text(width,(height*0.015).to_i,"text-anchor"=>"end","font-family"=>"sans","font-size"=>(height*0.02).to_i,:fill=>AsciiDocColor){raw assessment}
          else
            assessment = "Selbsteinschätzung durch #{r[:pupilid]}, #{r[:date]}"
            text(0,(height*0.01).to_i,"text-anchor"=>"left","font-family"=>"sans","font-size"=>(height*0.01).to_i){raw copymark}
            text(0,(height*0.045).to_i,"text-anchor"=>"left","font-family"=>"sans","font-size"=>(height*0.025).to_i){raw assessment}
          end
        else assayer = r[:assayer]
          if z != 3
            assessment = "Einschätzung durch die Klassenleitung #{r[:assayer]}"
            text((width/2).to_i,(height*0.995).to_i,:fill=>AsciiDocColor,"text-anchor"=>"middle","font-family"=>"sans","font-size"=>(height*0.017).to_i){raw copymark}
            text(width,(height*0.015).to_i,"text-anchor"=>"end","font-family"=>"sans","font-size"=>(height*0.02).to_i,:fill=>AsciiDocColor){raw assessment}
          else
            assessment = "Einschätzung von #{r[:pupilid]} durch die Klassenleitung #{r[:assayer]}, #{r[:date]}"
            text(0,(height*0.01).to_i,"text-anchor"=>"left","font-family"=>"sans","font-size"=>(height*0.01).to_i){raw copymark}
            text(0,(height*0.045).to_i,"text-anchor"=>"left","font-family"=>"sans","font-size"=>(height*0.025).to_i){raw assessment}
          end
        end

      end

      if r[:assayer].strip == "self"
        y = "pupil"
      else
        y = r[:assayer].strip.downcase
      end

      File.open(AKYCDIR.tempdirs[3] + "/#{r[:pupilid][0..7]}-#{r[:date]}-#{y}-v#{z}.svg", "w") { |f|
        img.write(f)
      }

    }
  }
