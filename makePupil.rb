#!/usr/bin/env ruby
require 'csv'
require 'fileutils'
require_relative 'whatComp'
require_relative 'whatDir'
require_relative 'whatFile'

  pupil = ARGV[0]
  INfile = ONEpupil

  if !File.exists?(ALLpupil)
    puts ALLpupil + " does not exist!"
    AKYCDIR.statdirs.each { |i|
      if !File.exists?(i)
        Dir.mkdir(i,0755)
        puts "made directory: #{i}"
      else
        puts "directory: #{i} exists, nothing to change"
      end
    }

    if File.exists?('simple.csv')
      FileUtils.cp 'simple.csv', ALLpupil
      if File.exists?(ALLpupil)
        puts ALLpupil + " does exist as dummy, please fill with real data!"
      end
    else
      puts ALLpupil + " does not exist!"
      puts "That is a real problem!"
      puts "does simple.csv exist?"
      puts "If it does not, do"
      puts "git checkout simple.csv"
      exit
    end
  end

  header  = "date,pupilid,c01,c02,c03,c04,c05,c06,c07,c08,c09,c10,c11,c12,c13,c14,assayer,pupil,sex"
  k = 0

  CSV.foreach(ALLpupil, headers:true, header_converters: :symbol, converters: :integer) { |r|
    if r[:pupil] == pupil
      k = k + 1
    end
  }

  if k < 1
    puts "Pupil \"#{pupil}\" does not exist!"
    exit
  end

  f = File.new(ONEpupil, "w")
    f.write(header + "\n")
  f.close

  CSV.foreach(ALLpupil, headers:true, header_converters: :symbol, converters: :integer) { |r|
    if r[:pupil] == pupil
      CSV.open(ONEpupil,"a") { |l|
	    l << r
	  }
    end
  }

  require_relative 'makePages'
