#!/usr/bin/env ruby
require_relative 'whatDir'

  AKYCDIR.tempdirs.each { |i|
    if !File.exists?(i)
      Dir.mkdir(i,0755)
      puts "made directory: #{i}"
    else
      puts "directory: #{i} exists, nothing to change"
    end
  }

  AKYCDIR.statdirs.each { |i|
    if !File.exists?(i)
      Dir.mkdir(i,0755)
      puts "made directory: #{i}"
    else
      puts "directory: #{i} exists, nothing to change"
    end
  }
