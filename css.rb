#!/usr/bin/env ruby
# encoding: utf-8
require_relative 'makeDir'
require_relative 'whatComp'

file = AKYCDIR.tempdirs[2] + "/adoc.css"

if !File.exists?(file)
  f = File.new(file, "w")
    f.write("html {font-family:\"DejaVu Sans\",\"Open Sans\",sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%} \n")
    f.write("body {padding:10;margin:10} \n")
    f.write("a:link {color:#DAA520; text-decoration: underline;} \n")
    f.write("a:visited {color:#E7E7E7; text-decoration: underline #DAA520;} \n")
    f.write("img {display:block;} \n")
    f.write("h1,h2,h3,h4 {text-decoration: none; position: relative; padding:20; margin:20;font-weight:400;font-style:normal;color:" + AsciiDocColor + ";margin-top:1em;margin-bottom:.5em;line-height:1.0125em} \n")
    f.write("h1{font-size:2.6em} \n")
    f.write("h2{font-size:2.2em;text-align:center} \n")
    f.write("h3{font-size:1.6em;text-align:center} \n")
    f.write("h4,h5,h6 {font-size:1.0em; margin-left:20%;} \n")
    f.write("table,th,td {border-collapse:collapse;border-spacing:0;margin:0;padding:0;}\n")
    f.write("*.tableblock:last-child {margin-top:0em;margin-bottom:0em;padding:0;border-spacing:0}\n")
    f.write(".center {margin:auto;} \n")
    f.write(".line {clear:both;margin-left:15px;margin-right:15px;margin-top:25px;margin-bottom:25px;text-align: center; background-color:" + AsciiDocColor + "; height:8px;font-size:6px; color:#ffffff; transition: transform 2s;} \n")
    f.write(".keyh1 {display:block; position: sticky; top: 40px; left:15px; margin-left:15px; margin-right:330px;text-align: center; color:" + AsciiDocColor + "; height:20px;font-size:2.0em; } \n")
    f.write(".keyh2 {display:block; position: sticky; top: 100px; left:15px; margin-left:15px; margin-right:330px;text-align: center; color:" + AsciiDocColor + "; height:20px;font-size:1.40em; } \n")
    f.write(".keyh3 {position: fixed; top: 120px; left:30px; width:200px; text-align: left; color:#e7e7e7; font-size:122px; transform: rotate(-5deg) } \n")
    f.write(".keyline {display:block;position: sticky; top: 180px; left:15px; margin-left:15px; margin-right:330px;text-align: center; background-color:" + AsciiDocColor + "; height:8px;font-size:6px; color:#ffffff; transition: transform 2s;} \n")
    
    (1..14).each_with_index {|i,j|
    case j
      when 0
        t = 120
        l = 20
        r = -10
      when 1
        t = 180
        l = 220
        r = 5
      when 3
        t = 220
        l = 420
        r = -5
      when 4
        t = 150
        l = 620
        r = 3
      when 5
        t = 250
        l = 5
        r = 6
      when 6
        t = 310
        l = 110
        r = -3
      when 7
        t = 295
        l = 410
        r = 7
      when 8
        t = 240
        l = 650
        r = -18
      when 9
        t = 395
        l = 60
        r = -20
      when 10
        t = 380
        l = 265
        r = 9
      when 11
        t = 420
        l = 510
        r = -4
      when 12
        t = 485
        l = 610
        r = 6
      when 13
        t = 540
        l = 120
        r = 18
      when 2
        t = 590
        l = 460
        r = -14
      else
        puts "!!!"
        puts "look at your css.rb file!"
        puts "use parameter from 0 to 13, no other are allowed"
        exit 
      end

      f.write(".keytext" + "%.2d" % i.to_s + " {")
      f.write("display:block;")
      f.write("position: absolute;")
      f.write("top: " + t.to_s + "px;")
      f.write("left: " + l.to_s + "px;")
      f.write("color:#e7e7e7; background-color:" + AsciiDocColor + ";")
      f.write("height:30px;")
      f.write("width:300px;")
      f.write("vertical-align: middle;")
      f.write("padding: 10px;")
      f.write("font-size:1.40em;")
      f.write("transform: rotate(" + r.to_s + "deg);")
      f.write("opacity: 0.6;")
      f.write("transition: transform 0.4s;")
      f.write("} \n")
      f.write(" \n")
      f.write(".keytext" + "%.2d" % i.to_s + ":hover{ \n")
      f.write("z-index: 1; \n")
      f.write("color:#000000; \n")
      f.write("background-color:#e1e1e1; \n")
      f.write("font-size:1.6em;")
      f.write("transform: scale(1.8) translate(60px); \n")
      f.write("opacity:1.0} \n")
      f.write(" \n")

      f.write(".keycontent" + "%.2d" % i.to_s + " {")
      f.write("display:inline-block;")
      f.write("position: absolute;")
      f.write("top: " + t.to_s + "px;")
      f.write("left: " + l.to_s + "px;")
      f.write("color:#e7e7e7; background-color:" + AsciiDocColor + ";")
      f.write("height:5%;")
      f.write("width:30%;")
      f.write("vertical-align: middle;")
      f.write("padding: 5%;")
      f.write("font-size:1.20em;")
      f.write("transform: rotate(" + r.to_s + "deg);")
      f.write("opacity: 0.6;")
      f.write("transition: transform 0.4s;")
      f.write("} \n")
      f.write(" \n")
      f.write(".keycontent" + "%.2d" % i.to_s + ":hover{ \n")
      f.write("z-index: 1; \n")
      f.write("color:#000000; \n")
      f.write("background-color:#e9e9e9; \n")
      f.write("padding-bottom: 6%;")
      f.write("font-size:1.4em;")
      f.write("transform: scale(1.2) translate(10%); \n")
      f.write("opacity:0.8} \n")
      f.write(" \n")

    }
    
    f.write(".line:hover {z-index:2;transform: scale(2);} \n")
    f.write(".keyline:hover {z-index:2;transform: scale(3) translate(50px);} \n")
    f.write(".floatleft {margin-left:20%;float: left; background-color:#e1e1e1; transition: transform .3s;} \n")
    f.write(".floatright {margin-right:20%;float: right;background-color:#e7e7e7; transition: transform .3s;} \n")
    f.write(".floatleft:hover {transform: scale(1.3);} \n") 
    f.write(".floatright:hover {transform: scale(1.4);} \n")
    f.write(".topcell {z-index:-1; position: absolute; top:5px; right:0px; margin-right:0px;width:300px;height:" + HEIGHT.to_s + "px; padding:5px; clear: right; float: right; background-color:#f7f7f7; color:" + AsciiDocColor + "; font-size:1em} \n")
    f.write(".keycell {z-index:1; position: relative; top: -50px; margin-right:-48px; margin-top:-15px; clear: both; float: right;background-color:#ffffff; transform: scale(0.6); transition: transform .3s;} \n")
    f.write(".keycell:hover {z-index: 2; background-color:#e7e7e7; transform: translate(-40px, -10px) scale(0.9) rotate(0.7deg);opacity: 0.9;} \n")
    f.write(".bottomleft {position: fixed; left: 20%; bottom: -20px; background-color:#C7C7C7; width:20%;height:40px;opacity: 0.5; text-align:center; transition: transform 0.2s;} \n")
    f.write(".bottomcenter {position: fixed; left:40%; bottom: -20px; background-color:#C7C7C7; width:20%;height:40px;opacity: 0.5; text-align:center; transition: transform 0.2s;} \n")
    f.write(".bottomright {position: fixed;  left:60%; bottom: -20px; background-color:#C7C7C7; width:20%;height:40px;opacity: 0.5; text-align:center; transition: transform 0.2s;} \n")
    f.write(".bottomleft:hover,.bottomcenter:hover,.bottomright:hover {z-index: 1; background-color:black;opacity: 0.6; transform: translate(0px,-20px)} \n")
    f.write(".topright {position: fixed; top: 0.8em;right: 0.1em} \n")
    f.write(".small {font-size: 12px} \n")
  f.close
end
