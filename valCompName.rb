#!/usr/bin/env ruby
# encoding: utf-8
require_relative 'whatDir'
require_relative 'whatComp'
require 'rasem'
# https://github.com/aseldawy/rasem
# numbers for key competences (only web-pages)

  height = HEIGHT       # change HEIGHT in whatComp.rb
  width = height/16     # col width for nested tables first col
  y0 = height/13        # height of dummy for nested tables first row
  y14 = height*0.9      # height over all, for nested tables 14 numbered rows 2 to 15
  y = y14/14            # height of one row (nested tables rows 2 to 15)
  y15 = height-(y0+y14) # height of dummy for nested tables last row

  file = AKYCDIR.tempdirs[3] + "/n0.svg" # placeholder,dummy-image
  img = Rasem::SVGImage.new(:width => width, :height => y0) {
  }

  if !File.exists?(file)
    File.open(file, "w") { |f|
      img.write(f)
    }
  end

  file = AKYCDIR.tempdirs[3] + "/n15.svg" # placeholder,dummy-image
  img = Rasem::SVGImage.new(:width => width, :height => y15) {
  }

  if !File.exists?(file)
    File.open(file, "w") { |f|
      img.write(f)
    }
  end

  CompetenceNames.each {|(val,key)|  # numbers for key competences
    file = AKYCDIR.tempdirs[3] + "/ValCompName" + val + ".svg"
    img = Rasem::SVGImage.new(:width => width, :height => y) {
      text(height/20,y/2,fill:"gray","dominant-baseline"=>"central","text-anchor"=>"end","font-family"=>"sans","font-size"=>y*0.45){raw val}
      line(height/19,y/2,width,y/2,:stroke=>"lightgray",:stroke_width=>(height*0.003).to_i)
      line( 0,y/2,height/80,y/2,:stroke=>"lightgray",:stroke_width=>(height*0.003).to_i)
    }

    if !File.exists?(file)
      File.open(file, "w") { |f|
        img.write(f)
      }
    end
  }
