#!/usr/bin/env ruby
# encoding: utf-8
require_relative 'whatDir'
require_relative 'whatComp'
require 'rasem'
# https://github.com/aseldawy/rasem
# short uuid as image

  def uuid(filename)
    height = HEIGHT/13  # change HEIGHT in whatComp.rb
    width = 300

    file = AKYCDIR.tempdirs[3] + "/" + filename[0..7] + ".svg"
    img = Rasem::SVGImage.new(:width => width, :height => height) {
      rectangle(10,5,width/1.4,height/1.8,:stroke=>AsciiDocColor,:stroke_width=>3,:fill=>"white").rotate(-6).translate(-5,20)
      text(100,60,"font-family"=>"sans",:fill=>AsciiDocColor,"font-size"=>30,"text-anchor"=>"middle"){raw filename[0..7]}.rotate(-6).translate(10,-6)
    }

    if !File.exists?(file)
      File.open(file, "w") { |f|
        img.write(f)
      }
    end

  end
